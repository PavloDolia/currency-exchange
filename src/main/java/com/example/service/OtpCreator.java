package com.example.service;

public class OtpCreator {

    public static String generateOtp() {
        int max = 999999;
        int min = 100100;
        int number = (int) ((Math.random() * (max - min)) + min);
        return String.valueOf(number);
    }

}