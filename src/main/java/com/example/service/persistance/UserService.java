package com.example.service.persistance;


import com.example.persistence.entity.UserEntity;
import com.example.pojo.User;

import java.util.Optional;

public interface UserService {

    Optional<UserEntity> findByLogin(String login);

    Optional<UserEntity> findById(Long id);

    Optional<UserEntity> save(User user);

    void deleteById(Long id);

    void deleteByLogin(String login);

}