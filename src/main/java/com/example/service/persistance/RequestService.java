package com.example.service.persistance;

import com.example.persistence.entity.ExchangeRequestEntity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface RequestService {

    Optional<ExchangeRequestEntity> findById(Long id);

    ExchangeRequestEntity saveOrUpdate(ExchangeRequestEntity request);

    void updateStatus(Long id, String status);

    boolean delete(Long id);

    List<ExchangeRequestEntity> findDoneTodayRequests();

    List<ExchangeRequestEntity> findDoneRequestsForCurrencyBetweenDates(LocalDateTime from,
                                                                        LocalDateTime to,
                                                                        String currencyCode);

}