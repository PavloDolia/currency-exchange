package com.example.controller;

import com.example.pojo.User;
import com.example.service.AuthorizationService;
import com.example.validator.UserValidator;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/auth")
@Tag(name = "Authorization controller", description = "It is used to authorise users")
public class AuthController {
    AuthorizationService authService;

    public AuthController(AuthorizationService authService) {
        this.authService = authService;
    }

    @InitBinder
    public void noteInitBinder(WebDataBinder binder) {
        binder.addValidators(new UserValidator());
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "If the login and password are correct",
                    headers = @Header(name = HttpHeaders.AUTHORIZATION,
                            description = "an authorization token in format: \"Bearer: header.payload.signature\""),
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "If the login or password is empty or if the login is too long",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "If the login or password is incorrect",
                    content = {@Content(mediaType = "raw",
                            examples = @ExampleObject(value = "Incorrect login or password"))})
    })
    @PostMapping("/login")
    public ResponseEntity login(@RequestBody @Validated User user) {
        return authService.login(user);
    }

    @PostMapping("/register")
    @Hidden
    public ResponseEntity register(@RequestBody @Validated User user) {
        return authService.register(user);
    }

}