package com.example.validator;

import com.example.constants.ValidationConstants;
import com.example.pojo.ExchangeRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.math.BigDecimal;

public class ExchangeRequestValidator implements Validator {
    public static final String PHONE_NUMBER_REGEX = "^([0-9]{9,10})$";

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == ExchangeRequest.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        ExchangeRequest request = (ExchangeRequest) target;
        String currencyToSell = request.getCurrencyToSell() != null ?
                request.getCurrencyToSell().toUpperCase() : null;
        String currencyToBuy = request.getCurrencyToBuy() != null ?
                request.getCurrencyToBuy().toUpperCase() : null;
        validateCurrencyToSell(currencyToSell, errors);
        validateCurrencyToBuy(currencyToBuy, errors);
        validatePhoneNumber(request.getPhoneNumber(), errors);
        validateAmountToSell(request.getAmountToSell(), errors);
        checkEmailIsEmpty(request.getEmail(), errors);
    }

    private void validateCurrencyToSell(String currencyToSell, Errors errors) {
        if (currencyToSell == null || !ValidationConstants.CURRENCY_CODES.contains(currencyToSell)) {
            errors.rejectValue("currencyToSell", "", "Incorrect currency code");
        }
    }

    private void validateCurrencyToBuy(String currencyToBuy, Errors errors) {
        if (currencyToBuy == null || !ValidationConstants.CURRENCY_CODES.contains(currencyToBuy)) {
            errors.rejectValue("currencyToBuy", "", "Incorrect currency code");
        }
    }

    private void validatePhoneNumber(String phoneNumber, Errors errors) {
        if (phoneNumber == null) {
            errors.rejectValue("phoneNumber", "", "Enter phone number");
        }
        if (phoneNumber != null) {
            int phoneNumberLength = phoneNumber.length();
            if (phoneNumberLength > 10 || phoneNumberLength < 9
                    || !phoneNumber.matches(PHONE_NUMBER_REGEX)) {
                errors.rejectValue("phoneNumber", "", "Incorrect phone number format");
            }
        }
    }

    private void validateAmountToSell(BigDecimal amountToSell, Errors errors) {
        if (amountToSell == null
                || amountToSell.compareTo(BigDecimal.ZERO) <= 0) {
            errors.rejectValue("amountToSell", "", "Incorrect amount of money to sell");
        }
    }

    private void checkEmailIsEmpty(String email, Errors errors) {
        if (StringUtils.isBlank(email)) {
            errors.rejectValue("email", "", "Enter email");
        }
    }

}