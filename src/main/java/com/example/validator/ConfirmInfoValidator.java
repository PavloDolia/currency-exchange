package com.example.validator;

import com.example.pojo.ConfirmInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class ConfirmInfoValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == ConfirmInfo.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        ConfirmInfo confirmInfo = (ConfirmInfo) target;
        validateId(confirmInfo.getId(), errors);
        validatePhoneNumber(confirmInfo.getPhoneNumber(), errors);
        validateOtp(confirmInfo.getOtp(), errors);
    }

    private void validateId(Long id, Errors errors) {
        if (id == null) {
            errors.rejectValue("id", "", "Enter id");
        }
        if (id != null && id <= 0) {
            errors.rejectValue("id", "",
                    "Incorrect id: id cannot be equal or less than zero");
        }
    }

    private void validatePhoneNumber(String phoneNumber, Errors errors) {
        if (StringUtils.isBlank(phoneNumber)) {
            errors.rejectValue("phoneNumber", "", "Enter phone number");
        }
    }

    private void validateOtp(String otp, Errors errors) {
        if (StringUtils.isBlank(otp)) {
            errors.rejectValue("otp", "", "Enter otp");
        }
    }

}