package com.example.persistence.entity;

import com.example.pojo.ExchangeRequest;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "exchangeRequests")
public class ExchangeRequestEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "phone", length = 20)
    private String phoneNumber;

    @Column(name = "sellType", length = 5)
    private String currencyToSell;

    @Column(name = "buyType", length = 5)
    private String currencyToBuy;

    @Column(precision = 10, scale = 2)
    private BigDecimal amountToSell;

    @Column(precision = 10, scale = 2)
    private BigDecimal amountToBuy;

    @Column
    private String email;

    @Column(length = 6)
    private String otp;

    @Column(length = 20)
    private String status;

    @Column(name = "date", nullable = false, columnDefinition = "DATETIME")
    private LocalDateTime date;

    public ExchangeRequestEntity() {
        date = LocalDateTime.now();
    }

    public ExchangeRequestEntity(ExchangeRequest request, String otp, BigDecimal amountToBuy) {
        phoneNumber = request.getPhoneNumber();
        currencyToSell = request.getCurrencyToSell().toUpperCase();
        currencyToBuy = request.getCurrencyToBuy().toUpperCase();
        amountToSell = request.getAmountToSell();
        email = request.getEmail();
        this.otp = otp;
        this.amountToBuy = amountToBuy;
        status = "new";
        date = LocalDateTime.now();
    }

    public Long getId() {
        return id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getCurrencyToSell() {
        return currencyToSell;
    }

    public String getCurrencyToBuy() {
        return currencyToBuy;
    }

    public BigDecimal getAmountToSell() {
        return amountToSell;
    }

    public BigDecimal getAmountToBuy() {
        return amountToBuy;
    }

    public String getEmail() {
        return email;
    }

    public String getOtp() {
        return otp;
    }

    public String getStatus() {
        return status;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setCurrencyToSell(String currencyToSell) {
        this.currencyToSell = currencyToSell;
    }

    public void setCurrencyToBuy(String currencyToBuy) {
        this.currencyToBuy = currencyToBuy;
    }

    public void setAmountToSell(BigDecimal amountToSell) {
        this.amountToSell = amountToSell;
    }

    public void setAmountToBuy(BigDecimal amountToBuy) {
        this.amountToBuy = amountToBuy;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

}