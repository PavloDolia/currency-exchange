package com.example.persistence.repository;

import com.example.persistence.entity.CurrencyRatesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CurrencyRepository extends JpaRepository<CurrencyRatesEntity, Long> {

    @Query("select c from CurrencyRatesEntity c where c.date = " +
            " (select max(c1.date) from CurrencyRatesEntity c1)")
    CurrencyRatesEntity findLastCurrencyRates();

}