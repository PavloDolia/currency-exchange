package com.example.persistence.repository;

import com.example.persistence.entity.ExchangeRequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface RequestRepository extends JpaRepository<ExchangeRequestEntity, Long> {

    @Modifying
    @Query("update ExchangeRequestEntity entity set entity.status=:status " +
            "where entity.id=:id")
    void updateStatus(@Param("id") Long id, @Param("status") String status);

    @Query("select entity from ExchangeRequestEntity entity " +
            "where entity.date>=:from and entity.date<=:to " +
            "and entity.status='done' order by entity.date")
    List<ExchangeRequestEntity> findDoneRequestsBetweenDates(@Param("from") LocalDateTime from,
                                                             @Param("to") LocalDateTime to);

    @Query("select entity from ExchangeRequestEntity entity " +
            "where entity.date>=:from and entity.date<=:to " +
            "and (entity.currencyToSell=:code or entity.currencyToBuy=:code) " +
            "and entity.status='done' order by entity.date")
    List<ExchangeRequestEntity> findDoneRequestsForCurrencyBetweenDates(@Param("from") LocalDateTime from,
                                                                        @Param("to") LocalDateTime to,
                                                                        @Param("code") String currencyCode);

    @Modifying
    @Query("delete ExchangeRequestEntity entity where entity.id=:id " +
            "and status='new'")
    int deleteIfStatusIsNew(@Param("id") Long id);

}