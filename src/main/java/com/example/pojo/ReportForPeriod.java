package com.example.pojo;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.v3.oas.annotations.media.Schema;

import java.math.BigDecimal;
import java.util.List;

@JsonPropertyOrder({"ccy", "soldTotal", "boughtTotal", "totalTransactions", "dayReports"})
public class ReportForPeriod {
    @JsonProperty("ccy")
    @Schema(description = "a currency code", example = "USD, EUR or UAH")
    private String currencyCode;

    @Schema(description = "the total amount of currency that has been sold for the specified period")
    private BigDecimal soldTotal;

    @Schema(description = "the total amount of currency that has been bought for the specified period")
    private BigDecimal boughtTotal;

    @Schema(description = "the total number of currency exchange transactions for the specified period")
    private int totalTransactions;
    private List<DayReport> dayReports;

    public ReportForPeriod() {
    }

    public ReportForPeriod(String currencyCode, BigDecimal soldTotal, BigDecimal boughtTotal,
                           int totalTransactions, List<DayReport> dayReports) {
        this.currencyCode = currencyCode;
        this.soldTotal = soldTotal;
        this.boughtTotal = boughtTotal;
        this.dayReports = dayReports;
        this.totalTransactions = totalTransactions;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getSoldTotal() {
        return soldTotal;
    }

    public void setSoldTotal(BigDecimal soldTotal) {
        this.soldTotal = soldTotal;
    }

    public BigDecimal getBoughtTotal() {
        return boughtTotal;
    }

    public void setBoughtTotal(BigDecimal boughtTotal) {
        this.boughtTotal = boughtTotal;
    }

    public int getTotalTransactions() {
        return totalTransactions;
    }

    public void setTotalTransactions(int totalTransactions) {
        this.totalTransactions = totalTransactions;
    }

    public List<DayReport> getDayReports() {
        return dayReports;
    }

    public void setDayReports(List<DayReport> dayReports) {
        this.dayReports = dayReports;
    }

}