package com.example.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import java.math.BigDecimal;

public class ExchangeRequest {
    @JsonProperty("sell")
    @Schema(description = "the code of the currency that a customer wants to exchange. It contains 3 characters",
            example = "USD, EUR or UAH")
    private String currencyToSell;

    @JsonProperty("buy")
    @Schema(description = "the code of the currency that a customer wants to buy. It contains 3 characters",
            example = "USD, EUR or UAH")
    private String currencyToBuy;

    @JsonProperty("moneyToSell")
    @Schema(description = "an amount of money that a customer gives")
    private BigDecimal amountToSell;

    @Schema(description = "a set of 9 or 10 digits", example = "0973742713 or 973742713")
    private String phoneNumber;
    private String email;

    public ExchangeRequest() {
    }

    public String getCurrencyToSell() {
        return currencyToSell;
    }

    public void setCurrencyToSell(String currencyToSell) {
        this.currencyToSell = currencyToSell;
    }

    public String getCurrencyToBuy() {
        return currencyToBuy;
    }

    public void setCurrencyToBuy(String currencyToBuy) {
        this.currencyToBuy = currencyToBuy;
    }

    public BigDecimal getAmountToSell() {
        return amountToSell;
    }

    public void setAmountToSell(BigDecimal amountToSell) {
        this.amountToSell = amountToSell;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}