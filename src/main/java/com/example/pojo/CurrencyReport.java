package com.example.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import java.math.BigDecimal;

public class CurrencyReport {
    @Schema(description = "an amount of money that has been given to customers")
    private BigDecimal sold;

    @Schema(description = "an amount of money that has been taken from customers")
    private BigDecimal bought;

    @JsonProperty("ccy")
    @Schema(description = "a currency code", example = "USD, EUR or UAH")
    private String currencyCode;

    public CurrencyReport() {
    }

    public CurrencyReport(BigDecimal sold, BigDecimal bought,
                          String currencyCode) {
        this.currencyCode = currencyCode;
        this.sold = sold;
        this.bought = bought;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getSold() {
        return sold;
    }

    public void setSold(BigDecimal sold) {
        this.sold = sold;
    }

    public BigDecimal getBought() {
        return bought;
    }

    public void setBought(BigDecimal bought) {
        this.bought = bought;
    }

}