package com.example.pojo;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

public class TodaysReport {
    @Schema(description = "a date in the format dd.MM.yyyy", example = "15.01.2022")
    private String date;

    @Schema(description = "the total number of currency exchange transactions")
    private int transactionCount;
    private List<CurrencyReport> currencyReports;

    public TodaysReport() {
    }

    public TodaysReport(String date, int transactionCount, List<CurrencyReport> currencyReports) {
        this.date = date;
        this.transactionCount = transactionCount;
        this.currencyReports = currencyReports;
    }

    public List<CurrencyReport> getCurrencyReports() {
        return currencyReports;
    }

    public void setCurrencyReports(List<CurrencyReport> currencyReports) {
        this.currencyReports = currencyReports;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionCount(int transactionCount) {
        this.transactionCount = transactionCount;
    }

}