package com.example.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

public class ConfirmInfo {
    @Schema(description = "the identification number for a request")
    private Long id;

    @Schema(description = "a set of 9 or 10 numbers", example = "0973742713 or 973742713")
    private String phoneNumber;

    @JsonProperty(value = "OTP")
    @Schema(description = "a password to confirm a request. It contains 6 numbers", example = "123456")
    private String otp;

    public ConfirmInfo() {
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}