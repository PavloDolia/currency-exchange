package com.example.service;

import com.example.persistence.entity.CurrencyRatesEntity;
import com.example.persistence.repository.CurrencyRepository;
import com.example.service.persistance.CurrencyService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;

@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CurrencyRatesReceiverTest {
    @Autowired
    private CurrencyRatesReceiver currencyRatesReceiver;
    @Autowired
    private CurrencyService currencyService;
    @Autowired
    private CurrencyRepository repository;

    @Test
    public void saveCurrencyRates_shouldReturnCorrectEntity() {
        CurrencyRatesEntity entity = currencyRatesReceiver.saveCurrencyRates();
        Assertions.assertTrue(entity.getDollarBuyingRate().compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(entity.getDollarSellingRate().compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(entity.getEuroBuyingRate().compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(entity.getEuroSellingRate().compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(entity.getDate().toInstant().atZone(ZoneId.systemDefault())
                .toLocalDate().isEqual(LocalDate.now()));
    }

    @Test
    public void saveCurrencyRates_shouldSaveEntity() {
        repository.deleteAll();
        currencyRatesReceiver.saveCurrencyRates();
        CurrencyRatesEntity savedEntity = currencyService.findLastCurrencyRates();
        Assertions.assertTrue(savedEntity.getDollarBuyingRate().compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(savedEntity.getDollarSellingRate().compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(savedEntity.getEuroBuyingRate().compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(savedEntity.getEuroSellingRate().compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(savedEntity.getDate().toInstant().atZone(ZoneId.systemDefault())
                .toLocalDate().isEqual(LocalDate.now()));
    }

}