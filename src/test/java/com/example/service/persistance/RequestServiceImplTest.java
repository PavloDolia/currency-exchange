package com.example.service.persistance;

import com.example.persistence.entity.ExchangeRequestEntity;
import com.example.persistence.repository.RequestRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
public class RequestServiceImplTest {
    @Autowired
    private RequestServiceImpl requestService;
    @Autowired
    private RequestRepository jpaRepository;
    ExchangeRequestEntity requestEntity = new ExchangeRequestEntity();

    @BeforeEach
    public void prepareDataBeforeEach() {
        requestEntity.setId(null);
        requestEntity.setPhoneNumber("0973742713");
        requestEntity.setCurrencyToSell("UAH");
        requestEntity.setCurrencyToBuy("USD");
        requestEntity.setAmountToSell(new BigDecimal(2800));
        requestEntity.setAmountToBuy(new BigDecimal(100));
        requestEntity.setEmail("dartmolokowot@gmail.com");
        requestEntity.setOtp("123456");
        requestEntity.setStatus("new");
        requestEntity.setDate(LocalDateTime.now());
        jpaRepository.deleteAll();
    }

    @Test
    public void saveOrUpdate_shouldSaveEntity() {
        ExchangeRequestEntity savedEntity = requestService.saveOrUpdate(requestEntity);
        Optional<ExchangeRequestEntity> optionalEntity = requestService.findById(savedEntity.getId());
        Assertions.assertTrue(optionalEntity.isPresent());
    }

    @Test
    public void saveOrUpdate_shouldUpdateEntity() {
        ExchangeRequestEntity savedEntity = requestService.saveOrUpdate(requestEntity);
        String newEmail = "baeldung@gmail.com";
        savedEntity.setEmail(newEmail);
        savedEntity = requestService.saveOrUpdate(requestEntity);
        ExchangeRequestEntity updatedEntity = requestService.findById(savedEntity.getId()).get();
        Assertions.assertEquals(newEmail, updatedEntity.getEmail());
    }

    @Test
    public void updateStatus_Test() {
        ExchangeRequestEntity savedEntity = requestService.saveOrUpdate(requestEntity);
        String newStatus = "changedStatus";
        requestService.updateStatus(savedEntity.getId(), newStatus);
        ExchangeRequestEntity updatedEntity = requestService.findById(savedEntity.getId()).get();
        Assertions.assertEquals(newStatus, updatedEntity.getStatus());
    }

    @Test
    public void delete_Test() {
        ExchangeRequestEntity savedEntity = requestService.saveOrUpdate(requestEntity);
        requestService.delete(savedEntity.getId());
        Optional<ExchangeRequestEntity> updatedEntity = requestService.findById(savedEntity.getId());
        Assertions.assertFalse(updatedEntity.isPresent());
    }

    @Test
    public void findById_Test() {
        ExchangeRequestEntity savedEntity = requestService.saveOrUpdate(requestEntity);
        ExchangeRequestEntity foundEntity = requestService.findById(savedEntity.getId()).get();
        Assertions.assertTrue(foundEntity.getId() != null && foundEntity.getId() > 0);
        Assertions.assertEquals(requestEntity.getPhoneNumber(), foundEntity.getPhoneNumber());
        Assertions.assertEquals(requestEntity.getCurrencyToSell(), foundEntity.getCurrencyToSell());
        Assertions.assertEquals(requestEntity.getCurrencyToBuy(), foundEntity.getCurrencyToBuy());
        Assertions.assertEquals(requestEntity.getAmountToBuy(), foundEntity.getAmountToBuy().setScale(0));
        Assertions.assertEquals(requestEntity.getAmountToSell(), foundEntity.getAmountToSell().setScale(0));
        Assertions.assertEquals(requestEntity.getEmail(), foundEntity.getEmail());
        Assertions.assertEquals(requestEntity.getOtp(), foundEntity.getOtp());
        Assertions.assertEquals(requestEntity.getStatus(), foundEntity.getStatus());
        Assertions.assertTrue(requestEntity.getDate().isEqual(foundEntity.getDate()));
    }

    @Test
    public void findDoneRequestsForCurrencyBetweenDates_Test() {
        requestEntity.setStatus("done");
        requestService.saveOrUpdate(requestEntity);
        requestEntity.setDate(LocalDateTime.now().minusDays(1));
        requestEntity.setId(null);
        requestService.saveOrUpdate(requestEntity);
        requestEntity.setDate(LocalDateTime.now().minusDays(2));
        requestEntity.setId(null);
        requestService.saveOrUpdate(requestEntity);
        LocalDateTime from = LocalDate.now().atStartOfDay().minusDays(3);
        LocalDateTime to = LocalDate.now().atTime(LocalTime.MAX);
        List<ExchangeRequestEntity> requestList = requestService
                .findDoneRequestsForCurrencyBetweenDates(from, to, "USD");

        Assertions.assertEquals(3, requestList.size());
        Assertions.assertTrue(requestList.get(0).getDate().toLocalDate()
                .isEqual(LocalDate.now().minusDays(2)));
        Assertions.assertTrue(requestList.get(1).getDate().toLocalDate()
                .isEqual(LocalDate.now().minusDays(1)));
        Assertions.assertTrue(requestList.get(2).getDate().toLocalDate()
                .isEqual(LocalDate.now()));
        Assertions.assertTrue(requestList.get(0).getCurrencyToSell().equals("USD")
                || requestList.get(0).getCurrencyToBuy().equals("USD"));
        Assertions.assertTrue(requestList.get(1).getCurrencyToSell().equals("USD")
                || requestList.get(1).getCurrencyToBuy().equals("USD"));
        Assertions.assertTrue(requestList.get(2).getCurrencyToSell().equals("USD")
                || requestList.get(2).getCurrencyToBuy().equals("USD"));
    }

    @Test
    public void findDoneTodayRequests_Test() {
        requestEntity.setStatus("done");
        for (int i = 0; i < 5; i++) {
            requestEntity.setId(null);
            requestService.saveOrUpdate(requestEntity);
        }
        requestEntity.setDate(LocalDateTime.now().minusDays(1));
        for (int i = 0; i < 5; i++) {
            requestEntity.setId(null);
            requestService.saveOrUpdate(requestEntity);
        }
        requestEntity.setDate(LocalDateTime.now().minusDays(2));
        for (int i = 0; i < 5; i++) {
            requestEntity.setId(null);
            requestService.saveOrUpdate(requestEntity);
        }

        List<ExchangeRequestEntity> requestList = requestService
                .findDoneTodayRequests();
        boolean isTodayRequests = false;
        for (ExchangeRequestEntity request : requestList) {
            if (request.getDate().toLocalDate().isEqual(LocalDate.now())) {
                isTodayRequests = true;
            } else {
                isTodayRequests = false;
                break;
            }
        }
        Assertions.assertEquals(5, requestList.size());
        Assertions.assertTrue(isTodayRequests);
    }

}