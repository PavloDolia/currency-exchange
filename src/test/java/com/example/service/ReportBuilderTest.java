package com.example.service;


import com.example.persistence.entity.ExchangeRequestEntity;
import com.example.pojo.CurrencyReport;
import com.example.pojo.TodaysReport;
import com.example.service.persistance.RequestService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ReportBuilderTest {
    private RequestService requestService;
    private ReportBuilder reportBuilder;

    @BeforeEach
    public void beforeEach() {
        requestService = Mockito.mock(RequestService.class);
        reportBuilder = new ReportBuilder(requestService);
    }

    @Test
    public void buildTodayReport_shouldReturnResult_ifThereAreNoRequests() {
        Mockito.when(requestService.findDoneTodayRequests()).thenReturn(new ArrayList<>());
        TodaysReport report = reportBuilder.buildTodayReport();
        List<CurrencyReport> currencyReportList = report.getCurrencyReports();
        CurrencyReport dollarReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("USD")).findFirst().get();
        CurrencyReport uahReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("UAH")).findFirst().get();
        CurrencyReport euroReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("EUR")).findFirst().get();

        Assertions.assertEquals(LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
                report.getDate());
        Assertions.assertEquals(0, report.getTransactionCount());
        Assertions.assertEquals(3, report.getCurrencyReports().size());
        Assertions.assertTrue(dollarReport.getBought().compareTo(BigDecimal.ZERO) == 0
                && dollarReport.getSold().compareTo(BigDecimal.ZERO) == 0);
        Assertions.assertTrue(uahReport.getBought().compareTo(BigDecimal.ZERO) == 0
                && uahReport.getSold().compareTo(BigDecimal.ZERO) == 0);
        Assertions.assertTrue(euroReport.getBought().compareTo(BigDecimal.ZERO) == 0
                && euroReport.getSold().compareTo(BigDecimal.ZERO) == 0);
    }

    @Test
    public void buildTodayReport_shouldReturnResult_ifThereIsOnlyBuyDollarsRequest() {
        List<ExchangeRequestEntity> requestEntityList = new ArrayList<>();
        requestEntityList.add(buy100DollarsWith2800Uah());
        Mockito.when(requestService.findDoneTodayRequests()).thenReturn(requestEntityList);

        TodaysReport report = reportBuilder.buildTodayReport();
        List<CurrencyReport> currencyReportList = report.getCurrencyReports();
        CurrencyReport dollarReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("USD")).findFirst().get();
        CurrencyReport uahReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("UAH")).findFirst().get();
        CurrencyReport euroReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("EUR")).findFirst().get();

        Assertions.assertEquals(LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
                report.getDate());
        Assertions.assertEquals(1, report.getTransactionCount());
        Assertions.assertEquals(3, report.getCurrencyReports().size());
        Assertions.assertTrue(dollarReport.getBought().compareTo(new BigDecimal("0")) == 0
                && dollarReport.getSold().compareTo(new BigDecimal("100")) == 0);
        Assertions.assertTrue(uahReport.getBought().compareTo(new BigDecimal("2800")) == 0
                && uahReport.getSold().compareTo(new BigDecimal("0")) == 0);
        Assertions.assertTrue(euroReport.getBought().compareTo(new BigDecimal("0")) == 0
                && euroReport.getSold().compareTo(new BigDecimal("0")) == 0);
    }

    @Test
    public void buildTodayReport_shouldReturnResult_ifThereIsOnlySellDollarsRequest() {
        List<ExchangeRequestEntity> requestEntityList = new ArrayList<>();
        requestEntityList.add(sell100DollarsAndGet2700Uah());
        Mockito.when(requestService.findDoneTodayRequests()).thenReturn(requestEntityList);

        TodaysReport report = reportBuilder.buildTodayReport();
        List<CurrencyReport> currencyReportList = report.getCurrencyReports();
        CurrencyReport dollarReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("USD")).findFirst().get();
        CurrencyReport uahReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("UAH")).findFirst().get();
        CurrencyReport euroReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("EUR")).findFirst().get();

        Assertions.assertEquals(LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
                report.getDate());
        Assertions.assertEquals(1, report.getTransactionCount());
        Assertions.assertEquals(3, report.getCurrencyReports().size());
        Assertions.assertTrue(dollarReport.getBought().compareTo(new BigDecimal("100")) == 0
                && dollarReport.getSold().compareTo(new BigDecimal("0")) == 0);
        Assertions.assertTrue(uahReport.getBought().compareTo(new BigDecimal("0")) == 0
                && uahReport.getSold().compareTo(new BigDecimal("2700")) == 0);
        Assertions.assertTrue(euroReport.getBought().compareTo(new BigDecimal("0")) == 0
                && euroReport.getSold().compareTo(new BigDecimal("0")) == 0);
    }

    @Test
    public void buildTodayReport_shouldReturnResult_ifThereIsOnlyBuyEurosRequest() {
        List<ExchangeRequestEntity> requestEntityList = new ArrayList<>();
        requestEntityList.add(buy100EurosWith3200Uah());
        Mockito.when(requestService.findDoneTodayRequests()).thenReturn(requestEntityList);

        TodaysReport report = reportBuilder.buildTodayReport();
        List<CurrencyReport> currencyReportList = report.getCurrencyReports();
        CurrencyReport dollarReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("USD")).findFirst().get();
        CurrencyReport uahReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("UAH")).findFirst().get();
        CurrencyReport euroReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("EUR")).findFirst().get();

        Assertions.assertEquals(LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
                report.getDate());
        Assertions.assertEquals(1, report.getTransactionCount());
        Assertions.assertEquals(3, report.getCurrencyReports().size());
        Assertions.assertTrue(dollarReport.getBought().compareTo(new BigDecimal("0")) == 0
                && dollarReport.getSold().compareTo(new BigDecimal("0")) == 0);
        Assertions.assertTrue(uahReport.getBought().compareTo(new BigDecimal("3200")) == 0
                && uahReport.getSold().compareTo(new BigDecimal("0")) == 0);
        Assertions.assertTrue(euroReport.getBought().compareTo(new BigDecimal("0")) == 0
                && euroReport.getSold().compareTo(new BigDecimal("100")) == 0);
    }

    @Test
    public void buildTodayReport_shouldReturnResult_ifThereIsOnlySellEurosRequest() {
        List<ExchangeRequestEntity> requestEntityList = new ArrayList<>();
        requestEntityList.add(sell100EurosAndGet3100Uah());
        Mockito.when(requestService.findDoneTodayRequests()).thenReturn(requestEntityList);

        TodaysReport report = reportBuilder.buildTodayReport();
        List<CurrencyReport> currencyReportList = report.getCurrencyReports();
        CurrencyReport dollarReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("USD")).findFirst().get();
        CurrencyReport uahReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("UAH")).findFirst().get();
        CurrencyReport euroReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("EUR")).findFirst().get();

        Assertions.assertEquals(LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
                report.getDate());
        Assertions.assertEquals(1, report.getTransactionCount());
        Assertions.assertEquals(3, report.getCurrencyReports().size());
        Assertions.assertTrue(dollarReport.getBought().compareTo(new BigDecimal("0")) == 0
                && dollarReport.getSold().compareTo(new BigDecimal("0")) == 0);
        Assertions.assertTrue(uahReport.getBought().compareTo(new BigDecimal("0")) == 0
                && uahReport.getSold().compareTo(new BigDecimal("3100")) == 0);
        Assertions.assertTrue(euroReport.getBought().compareTo(new BigDecimal("100")) == 0
                && euroReport.getSold().compareTo(new BigDecimal("0")) == 0);
    }

    @Test
    public void buildTodayReport_shouldReturnResult_ifThereAre4Requests() {
        List<ExchangeRequestEntity> requestEntityList = new ArrayList<>();
        requestEntityList.add(buy100DollarsWith2800Uah());
        requestEntityList.add(buy100EurosWith3200Uah());
        requestEntityList.add(sell100DollarsAndGet2700Uah());
        requestEntityList.add(sell100EurosAndGet3100Uah());
        Mockito.when(requestService.findDoneTodayRequests()).thenReturn(requestEntityList);

        TodaysReport report = reportBuilder.buildTodayReport();
        List<CurrencyReport> currencyReportList = report.getCurrencyReports();
        CurrencyReport dollarReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("USD")).findFirst().get();
        CurrencyReport uahReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("UAH")).findFirst().get();
        CurrencyReport euroReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("EUR")).findFirst().get();

        Assertions.assertEquals(LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
                report.getDate());
        Assertions.assertEquals(4, report.getTransactionCount());
        Assertions.assertEquals(3, report.getCurrencyReports().size());
        Assertions.assertTrue(dollarReport.getBought().compareTo(new BigDecimal("100")) == 0
                && dollarReport.getSold().compareTo(new BigDecimal("100")) == 0);
        Assertions.assertTrue(uahReport.getBought().compareTo(new BigDecimal("6000")) == 0
                && uahReport.getSold().compareTo(new BigDecimal("5800")) == 0);
        Assertions.assertTrue(euroReport.getBought().compareTo(new BigDecimal("100")) == 0
                && euroReport.getSold().compareTo(new BigDecimal("100")) == 0);
    }

    @Test
    public void buildTodayReport_shouldReturnResult_ifThereAre8Requests() {
        List<ExchangeRequestEntity> requestEntityList = new ArrayList<>();
        requestEntityList.add(buy100DollarsWith2800Uah());
        requestEntityList.add(buy100DollarsWith2800Uah());
        requestEntityList.add(buy100EurosWith3200Uah());
        requestEntityList.add(buy100EurosWith3200Uah());
        requestEntityList.add(sell100DollarsAndGet2700Uah());
        requestEntityList.add(sell100DollarsAndGet2700Uah());
        requestEntityList.add(sell100EurosAndGet3100Uah());
        requestEntityList.add(sell100EurosAndGet3100Uah());
        Mockito.when(requestService.findDoneTodayRequests()).thenReturn(requestEntityList);

        TodaysReport report = reportBuilder.buildTodayReport();
        List<CurrencyReport> currencyReportList = report.getCurrencyReports();
        CurrencyReport dollarReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("USD")).findFirst().get();
        CurrencyReport uahReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("UAH")).findFirst().get();
        CurrencyReport euroReport = currencyReportList.stream()
                .filter(r -> r.getCurrencyCode().equals("EUR")).findFirst().get();

        Assertions.assertEquals(LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
                report.getDate());
        Assertions.assertEquals(8, report.getTransactionCount());
        Assertions.assertEquals(3, report.getCurrencyReports().size());
        Assertions.assertTrue(dollarReport.getBought().compareTo(new BigDecimal("200")) == 0
                && dollarReport.getSold().compareTo(new BigDecimal("200")) == 0);
        Assertions.assertTrue(uahReport.getBought().compareTo(new BigDecimal("12000")) == 0
                && uahReport.getSold().compareTo(new BigDecimal("11600")) == 0);
        Assertions.assertTrue(euroReport.getBought().compareTo(new BigDecimal("200")) == 0
                && euroReport.getSold().compareTo(new BigDecimal("200")) == 0);
    }


    private ExchangeRequestEntity buy100DollarsWith2800Uah() {
        ExchangeRequestEntity request = new ExchangeRequestEntity();
        request.setCurrencyToSell("UAH");
        request.setCurrencyToBuy("USD");
        request.setAmountToSell(new BigDecimal("2800.00"));
        request.setAmountToBuy(new BigDecimal("100.00"));
        request.setStatus("done");
        return request;
    }

    private ExchangeRequestEntity sell100DollarsAndGet2700Uah() {
        ExchangeRequestEntity request = new ExchangeRequestEntity();
        request.setCurrencyToSell("USD");
        request.setCurrencyToBuy("UAH");
        request.setAmountToSell(new BigDecimal("100.00"));
        request.setAmountToBuy(new BigDecimal("2700.00"));
        request.setStatus("done");
        return request;
    }


    private ExchangeRequestEntity buy100EurosWith3200Uah() {
        ExchangeRequestEntity request = new ExchangeRequestEntity();
        request.setCurrencyToSell("UAH");
        request.setCurrencyToBuy("EUR");
        request.setAmountToSell(new BigDecimal("3200.00"));
        request.setAmountToBuy(new BigDecimal("100.00"));
        request.setStatus("done");
        return request;
    }

    private ExchangeRequestEntity sell100EurosAndGet3100Uah() {
        ExchangeRequestEntity request = new ExchangeRequestEntity();
        request.setCurrencyToSell("EUR");
        request.setCurrencyToBuy("UAH");
        request.setAmountToSell(new BigDecimal("100.00"));
        request.setAmountToBuy(new BigDecimal("3100.00"));
        request.setStatus("done");
        return request;
    }

}