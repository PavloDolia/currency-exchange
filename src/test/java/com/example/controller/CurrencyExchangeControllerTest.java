package com.example.controller;

import com.example.persistence.entity.ExchangeRequestEntity;
import com.example.pojo.*;
import com.example.security.JwtUtil;
import com.example.service.CurrencyRatesReceiver;
import com.example.service.persistance.RequestService;
import com.example.validator.ExchangeRequestValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:test.properties")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CurrencyExchangeControllerTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private RequestService requestService;
    @Autowired
    private CurrencyRatesReceiver currencyRatesReceiver;
    private static final String START_WORKING_DAY_URI = "/exchange/start";
    private static final String CREATE_CURRENCY_EXCHANGE_REQUEST_URI = "/exchange/request/new";
    private static final String CONFIRM_CURRENCY_EXCHANGE_REQUEST_URI = "/exchange/request/confirm";
    private static final String DELETE_REQUEST_URI = "/exchange/request/delete/";
    private static final String GET_REPORT_BETWEEN_DATES_URI = "/exchange/report/between/dates";
    private String userToken;
    private ExchangeRequest exchangeRequest = new ExchangeRequest();
    private ConfirmInfo confirmInfo = new ConfirmInfo();
    private ReportInfo reportInfo = new ReportInfo();

    @BeforeAll
    public void prepareData() {
        userToken = "Bearer " + jwtUtil.generateAccessToken("user");
        currencyRatesReceiver.saveCurrencyRates();

        ExchangeRequestEntity requestEntity = new ExchangeRequestEntity();
        requestEntity.setPhoneNumber("0973742713");
        requestEntity.setCurrencyToSell("UAH");
        requestEntity.setCurrencyToBuy("USD");
        requestEntity.setAmountToSell(new BigDecimal(2800));
        requestEntity.setAmountToBuy(new BigDecimal(100));
        requestEntity.setEmail("dartmolokowot@gmail.com");
        requestEntity.setOtp("123456");
        requestEntity.setStatus("new");
        requestService.saveOrUpdate(requestEntity);

        requestEntity.setStatus("done");
        for (int i = 0; i < 6; i++) {
            if (i == 2 || i == 4) {
                requestEntity.setDate(requestEntity.getDate().minusDays(1));
            }
            requestEntity.setId(null);
            requestService.saveOrUpdate(requestEntity);
        }
    }

    @BeforeEach
    public void createCorrectRequests() {
        exchangeRequest.setCurrencyToSell("UAH");
        exchangeRequest.setCurrencyToBuy("USD");
        exchangeRequest.setAmountToSell(BigDecimal.valueOf(2800));
        exchangeRequest.setPhoneNumber("0973742713");
        exchangeRequest.setEmail("dartmolokowot@gmail.com");

        confirmInfo.setId(1L);
        confirmInfo.setOtp("123456");
        confirmInfo.setPhoneNumber("0973742713");

        reportInfo.setFromDate("2022-01-01");
        reportInfo.setToDate("2022-12-31");
        reportInfo.setCurrencyCode("USD");

        requestService.updateStatus(1L, "new");
    }

    @Test
    public void startWorkingDay_shouldReturnStatus200AndCorrectBody_ifCurrencyRatesReceived() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(START_WORKING_DAY_URI)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andReturn();

        String jsonResponse = mvcResult.getResponse().getContentAsString();
        CurrencyRates currencyRates = objectMapper.readValue(jsonResponse, CurrencyRates.class);

        int actualStatus = mvcResult.getResponse().getStatus();
        String actualDate = currencyRates.getDate();
        List<CurrencyRate> currencyRatesList = currencyRates.getCurrencyRates();
        CurrencyRate dollar = currencyRatesList.stream()
                .filter(c -> c.getCurrencyCode().equals("USD")).findFirst().get();
        CurrencyRate euro = currencyRatesList.stream()
                .filter(c -> c.getCurrencyCode().equals("EUR")).findFirst().get();
        BigDecimal dollarBuyingRate = dollar.getBuyingRate();
        BigDecimal dollarSellingRate = dollar.getSellingRate();
        BigDecimal euroBuyingRate = euro.getBuyingRate();
        BigDecimal euroSellingRate = euro.getSellingRate();

        Assertions.assertEquals(HttpStatus.OK.value(), actualStatus);
        Assertions.assertEquals(new SimpleDateFormat("dd.MM.yyyy").format(new Date()),
                actualDate);
        Assertions.assertEquals(2, currencyRatesList.size());
        Assertions.assertTrue(dollarBuyingRate.compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(dollarSellingRate.compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(euroBuyingRate.compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(euroSellingRate.compareTo(BigDecimal.ZERO) > 0);
    }

    @Test
    public void startWorkingDay_shouldReturnStatus403_ifWithoutJwt() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get(START_WORKING_DAY_URI)
        ).andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void startWorkingDay_shouldReturnStatus403_ifIncorrectJwt() throws Exception {
        String jwtKey = jwtUtil.getKey();
        jwtUtil.setKey("someKey");
        String wrongToken = "Bearer " + jwtUtil.generateAccessToken("user");
        jwtUtil.setKey(jwtKey);

        mvc.perform(MockMvcRequestBuilders.get(START_WORKING_DAY_URI)
                .header(HttpHeaders.AUTHORIZATION, wrongToken)
        ).andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createCurrencyExchangeRequest_shouldReturnStatus400_ifCurrencyToSellCodeIsNull() throws Exception {
        exchangeRequest.setCurrencyToSell(null);
        mvc.perform(MockMvcRequestBuilders.post(CREATE_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(exchangeRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError("currencyToSell",
                                "Incorrect currency code")))));
    }

    @Test
    public void createCurrencyExchangeRequest_shouldReturnStatus400_ifCurrencyToSellCodeIsIncorrect() throws Exception {
        exchangeRequest.setCurrencyToSell("PLN");
        mvc.perform(MockMvcRequestBuilders.post(CREATE_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(exchangeRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError("currencyToSell",
                                "Incorrect currency code")))));
    }

    @Test
    public void createCurrencyExchangeRequest_shouldReturnStatus400_ifCurrencyToBuyCodeIsNull() throws Exception {
        exchangeRequest.setCurrencyToBuy(null);
        mvc.perform(MockMvcRequestBuilders.post(CREATE_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(exchangeRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError("currencyToBuy",
                                "Incorrect currency code")))));
    }

    @Test
    public void createCurrencyExchangeRequest_shouldReturnStatus400_ifCurrencyToBuyCodeIsIncorrect() throws Exception {
        exchangeRequest.setCurrencyToBuy("PLN");
        mvc.perform(MockMvcRequestBuilders.post(CREATE_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(exchangeRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError("currencyToBuy",
                                "Incorrect currency code")))));
    }

    @Test
    public void createCurrencyExchangeRequest_shouldReturnStatus400_ifPhoneNumberIsTooLong() throws Exception {
        exchangeRequest.setPhoneNumber("80973742713");
        mvc.perform(MockMvcRequestBuilders.post(CREATE_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(exchangeRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError
                                ("phoneNumber", "Incorrect phone number format")))));
    }

    @Test
    public void createCurrencyExchangeRequest_shouldReturnStatus400_ifPhoneNumberIsTooShort() throws Exception {
        exchangeRequest.setPhoneNumber("73742713");
        mvc.perform(MockMvcRequestBuilders.post(CREATE_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(exchangeRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError
                                ("phoneNumber", "Incorrect phone number format")))));
    }

    @Test
    public void createCurrencyExchangeRequest_shouldReturnStatus400_ifPhoneNumberContainsNotOnlyNumbers()
            throws Exception {
        exchangeRequest.setPhoneNumber("o973742713"); // phone number contains letter "o"
        mvc.perform(MockMvcRequestBuilders.post(CREATE_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(exchangeRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError
                                ("phoneNumber", "Incorrect phone number format")))));
    }

    @Test
    public void createCurrencyExchangeRequest_shouldReturnStatus400_ifPhoneNumberIsNull() throws Exception {
        exchangeRequest.setPhoneNumber(null);
        mvc.perform(MockMvcRequestBuilders.post(CREATE_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(exchangeRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError
                                ("phoneNumber", "Enter phone number")))));
    }

    @Test
    public void createCurrencyExchangeRequest_shouldReturnStatus400_ifAmountToSellIsZero() throws Exception {
        exchangeRequest.setAmountToSell(BigDecimal.ZERO);
        mvc.perform(MockMvcRequestBuilders.post(CREATE_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(exchangeRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError
                                ("amountToSell", "Incorrect amount of money to sell")))));
    }

    @Test
    public void createCurrencyExchangeRequest_shouldReturnStatus400_ifAmountToSellIsLessThanZero() throws Exception {
        exchangeRequest.setAmountToSell(BigDecimal.ZERO.subtract(BigDecimal.ONE));
        mvc.perform(MockMvcRequestBuilders.post(CREATE_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(exchangeRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError
                                ("amountToSell", "Incorrect amount of money to sell")))));
    }

    @Test
    public void createCurrencyExchangeRequest_shouldReturnStatus400_ifEmailIsNull() throws Exception {
        exchangeRequest.setEmail(null);
        mvc.perform(MockMvcRequestBuilders.post(CREATE_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(exchangeRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError("email", "Enter email")))));
    }

    @Test
    public void createCurrencyExchangeRequest_shouldReturnStatus400_ifEmailIsEmpty() throws Exception {
        exchangeRequest.setEmail(" ");
        mvc.perform(MockMvcRequestBuilders.post(CREATE_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(exchangeRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError("email", "Enter email")))));
    }

    @Test
    public void createCurrencyExchangeRequest_shouldReturnStatus200AndCorrectBody_ifCorrectExchangeRequest()
            throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(CREATE_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(exchangeRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andReturn();
        String jsonResponse = mvcResult.getResponse().getContentAsString();
        RequestResponse requestResponse = objectMapper.readValue(jsonResponse, RequestResponse.class);

        int actualStatus = mvcResult.getResponse().getStatus();
        BigDecimal moneyAmountToBuy = requestResponse.getMoneyAmountToBuy();
        String phoneNumber = requestResponse.getPhoneNumber();
        String otp = requestResponse.getOtp();
        Long requestId = requestResponse.getId();

        Assertions.assertEquals(HttpStatus.OK.value(), actualStatus);
        Assertions.assertTrue(moneyAmountToBuy != null
                && moneyAmountToBuy.compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(phoneNumber != null
                && phoneNumber.matches(ExchangeRequestValidator.PHONE_NUMBER_REGEX));
        Assertions.assertTrue(otp != null && otp.matches("^([0-9]{6})$"));
        Assertions.assertTrue(requestId > 0);
    }

    @Test
    public void createCurrencyExchangeRequest_shouldReturnStatus403_ifWithoutJwt() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post(CREATE_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(exchangeRequest))
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createCurrencyExchangeRequest_shouldReturnStatus403_ifIncorrectJwt() throws Exception {
        String jwtKey = jwtUtil.getKey();
        jwtUtil.setKey("someKey");
        String wrongToken = "Bearer " + jwtUtil.generateAccessToken("user");
        jwtUtil.setKey(jwtKey);

        mvc.perform(MockMvcRequestBuilders.post(CREATE_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(exchangeRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, wrongToken)
        ).andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void confirmCurrencyExchangeRequest_shouldReturnStatus400_ifIdIsNull() throws Exception {
        confirmInfo.setId(null);
        mvc.perform(MockMvcRequestBuilders.put(CONFIRM_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(confirmInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError("id", "Enter id")))));
    }

    @Test
    public void confirmCurrencyExchangeRequest_shouldReturnStatus400_ifIdIsZero() throws Exception {
        confirmInfo.setId(0L);
        mvc.perform(MockMvcRequestBuilders.put(CONFIRM_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(confirmInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError("id",
                                "Incorrect id: id cannot be equal or less than zero")))));
    }

    @Test
    public void confirmCurrencyExchangeRequest_shouldReturnStatus400_ifIdIsLessThanZero() throws Exception {
        confirmInfo.setId(-1L);
        mvc.perform(MockMvcRequestBuilders.put(CONFIRM_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(confirmInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError("id",
                                "Incorrect id: id cannot be equal or less than zero")))));
    }

    @Test
    public void confirmCurrencyExchangeRequest_shouldReturnStatus400_ifPhoneNumberIsNull() throws Exception {
        confirmInfo.setPhoneNumber(null);
        mvc.perform(MockMvcRequestBuilders.put(CONFIRM_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(confirmInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError("phoneNumber",
                                "Enter phone number")))));
    }

    @Test
    public void confirmCurrencyExchangeRequest_shouldReturnStatus400_ifPhoneNumberIsEmpty() throws Exception {
        confirmInfo.setPhoneNumber(" ");
        mvc.perform(MockMvcRequestBuilders.put(CONFIRM_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(confirmInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError("phoneNumber",
                                "Enter phone number")))));
    }

    @Test
    public void confirmCurrencyExchangeRequest_shouldReturnStatus400_ifOtpIsNull() throws Exception {
        confirmInfo.setOtp(null);
        mvc.perform(MockMvcRequestBuilders.put(CONFIRM_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(confirmInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError("otp",
                                "Enter otp")))));
    }

    @Test
    public void confirmCurrencyExchangeRequest_shouldReturnStatus400_ifOtpIsEmpty() throws Exception {
        confirmInfo.setOtp(" ");
        mvc.perform(MockMvcRequestBuilders.put(CONFIRM_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(confirmInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError("otp",
                                "Enter otp")))));
    }

    @Test
    public void confirmCurrencyExchangeRequest_shouldReturnStatus400_ifRequestNotFoundById() throws Exception {
        confirmInfo.setId(Long.MAX_VALUE);
        mvc.perform(MockMvcRequestBuilders.put(CONFIRM_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(confirmInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (new Message("Incorrect id, unable to find a request with this id at a database"))));
    }

    @Test
    public void confirmCurrencyExchangeRequest_shouldReturnStatus200AndCorrectBody_ifOtpMatches() throws Exception {
        mvc.perform(MockMvcRequestBuilders.put(CONFIRM_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(confirmInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (new Message("The request has been confirmed"))));
    }

    @Test
    public void confirmCurrencyExchangeRequest_shouldReturnStatus403_ifWithoutJwt() throws Exception {
        mvc.perform(MockMvcRequestBuilders.put(CONFIRM_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(confirmInfo))
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void confirmCurrencyExchangeRequest_shouldReturnStatus403_ifIncorrectJwt() throws Exception {
        String jwtKey = jwtUtil.getKey();
        jwtUtil.setKey("someKey");
        String wrongToken = "Bearer " + jwtUtil.generateAccessToken("user");
        jwtUtil.setKey(jwtKey);

        mvc.perform(MockMvcRequestBuilders.put(CONFIRM_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(confirmInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, wrongToken)
        ).andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void confirmCurrencyExchangeRequest_shouldReturnStatus400_ifOtpNotMatches() throws Exception {
        confirmInfo.setOtp("654321");
        mvc.perform(MockMvcRequestBuilders.put(CONFIRM_CURRENCY_EXCHANGE_REQUEST_URI)
                .content(objectMapper.writeValueAsString(confirmInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().is(406))
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (new Message("OTP doesn't match. The request has been canceled"))));
    }

    @Test
    public void deleteRequest_shouldReturnStatus200AndCorrectBody_ifRequestStatusIsNew() throws Exception {
        ExchangeRequestEntity requestEntity = new ExchangeRequestEntity();
        requestEntity.setPhoneNumber("0973742713");
        requestEntity.setCurrencyToSell("UAH");
        requestEntity.setCurrencyToBuy("USD");
        requestEntity.setAmountToSell(new BigDecimal(5600));
        requestEntity.setAmountToBuy(new BigDecimal(200));
        requestEntity.setEmail("dartmolokowot@gmail.com");
        requestEntity.setOtp("123456");
        requestEntity.setStatus("new");
        requestEntity = requestService.saveOrUpdate(requestEntity);

        mvc.perform(MockMvcRequestBuilders.delete(DELETE_REQUEST_URI + requestEntity.getId())
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (new Message("The request has been deleted"))));
    }

    @Test
    public void deleteRequest_shouldReturnStatus400_ifRequestNotExist() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete(DELETE_REQUEST_URI + Long.MAX_VALUE)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().is(406))
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (new Message("Unable to delete a request, " +
                                "it doesn't exist or has been confirmed/canceled"))));
    }

    @Test
    public void deleteRequest_shouldReturnStatus400_ifRequestStatusIsDone() throws Exception {
        ExchangeRequestEntity requestEntity = new ExchangeRequestEntity();
        requestEntity.setPhoneNumber("0973742713");
        requestEntity.setCurrencyToSell("UAH");
        requestEntity.setCurrencyToBuy("USD");
        requestEntity.setAmountToSell(new BigDecimal(5600));
        requestEntity.setAmountToBuy(new BigDecimal(200));
        requestEntity.setEmail("dartmolokowot@gmail.com");
        requestEntity.setOtp("123456");
        requestEntity.setStatus("done");
        requestEntity = requestService.saveOrUpdate(requestEntity);

        mvc.perform(MockMvcRequestBuilders.delete(DELETE_REQUEST_URI + requestEntity.getId())
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().is(406))
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (new Message("Unable to delete a request, " +
                                "it doesn't exist or has been confirmed/canceled"))));

        requestService.updateStatus(requestEntity.getId(), "new");
        requestService.delete(requestEntity.getId());
        //TODO
        System.out.println("REQUEST DELETED");
    }

    @Test
    public void deleteRequest_shouldReturnStatus403_ifWithoutJwt() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete(DELETE_REQUEST_URI + 1)
        ).andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteRequest_shouldReturnStatus403_ifIncorrectJwt() throws Exception {
        String jwtKey = jwtUtil.getKey();
        jwtUtil.setKey("someKey");
        String wrongToken = "Bearer " + jwtUtil.generateAccessToken("user");
        jwtUtil.setKey(jwtKey);

        mvc.perform(MockMvcRequestBuilders.delete(DELETE_REQUEST_URI + 1)
                .header(HttpHeaders.AUTHORIZATION, wrongToken)
        ).andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getTodayReport_shouldReturnStatus200AndCorrectBody() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/exchange/report")
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andReturn();
        String jsonResponse = mvcResult.getResponse().getContentAsString();
        TodaysReport todaysReport = objectMapper.readValue(jsonResponse, TodaysReport.class);

        int actualStatus = mvcResult.getResponse().getStatus();
        int actualExchangeCount = todaysReport.getTransactionCount();
        String actualDate = todaysReport.getDate();
        List<CurrencyReport> actualCurrencyReports = todaysReport.getCurrencyReports();
        CurrencyReport euro = actualCurrencyReports.stream()
                .filter(c -> c.getCurrencyCode().equals("EUR")).findFirst().get();
        CurrencyReport dollar = actualCurrencyReports.stream()
                .filter(c -> c.getCurrencyCode().equals("USD")).findFirst().get();
        CurrencyReport uah = actualCurrencyReports.stream()
                .filter(c -> c.getCurrencyCode().equals("UAH")).findFirst().get();

        Assertions.assertEquals(200, actualStatus);
        Assertions.assertEquals(2, actualExchangeCount);
        Assertions.assertEquals(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
                actualDate);
        Assertions.assertEquals(3, actualCurrencyReports.size());
        Assertions.assertEquals(BigDecimal.ZERO, dollar.getBought());
        Assertions.assertEquals(new BigDecimal(200).setScale(2), dollar.getSold());
        Assertions.assertEquals(new BigDecimal(5600).setScale(2), uah.getBought());
        Assertions.assertEquals(BigDecimal.ZERO, uah.getSold());
        Assertions.assertEquals(BigDecimal.ZERO, euro.getBought());
        Assertions.assertEquals(BigDecimal.ZERO, euro.getSold());
    }

    @Test
    public void getTodayReport_shouldReturnStatus403_ifWithoutJwt() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/exchange/report")
        ).andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getTodayReport_shouldReturnStatus403_ifIncorrectJwt() throws Exception {
        String jwtKey = jwtUtil.getKey();
        jwtUtil.setKey("someKey");
        String wrongToken = "Bearer " + jwtUtil.generateAccessToken("user");
        jwtUtil.setKey(jwtKey);

        mvc.perform(MockMvcRequestBuilders.get("/exchange/report")
                .header(HttpHeaders.AUTHORIZATION, wrongToken)
        ).andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getReportBetweenDates_shouldReturnStatus400_ifFromDateIsNull() throws Exception {
        reportInfo.setFromDate(null);
        mvc.perform(MockMvcRequestBuilders.post(GET_REPORT_BETWEEN_DATES_URI)
                .content(objectMapper.writeValueAsString(reportInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError("fromDate",
                                "Enter \"from date\"")))));
    }

    @Test
    public void getReportBetweenDates_shouldReturnStatus400_ifFromDateFormatIsIncorrect() throws Exception {
        reportInfo.setFromDate("22.02.2022");
        mvc.perform(MockMvcRequestBuilders.post(GET_REPORT_BETWEEN_DATES_URI)
                .content(objectMapper.writeValueAsString(reportInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError("fromDate",
                                "Incorrect date format")))));
    }

    @Test
    public void getReportBetweenDates_shouldReturnStatus400_ifToDateIsNull() throws Exception {
        reportInfo.setToDate(null);
        mvc.perform(MockMvcRequestBuilders.post(GET_REPORT_BETWEEN_DATES_URI)
                .content(objectMapper.writeValueAsString(reportInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError("toDate",
                                "Enter \"to date\"")))));
    }

    @Test
    public void getReportBetweenDates_shouldReturnStatus400_ifToDateFormatIsIncorrect() throws Exception {
        reportInfo.setToDate("22.12.2022");
        mvc.perform(MockMvcRequestBuilders.post(GET_REPORT_BETWEEN_DATES_URI)
                .content(objectMapper.writeValueAsString(reportInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError("toDate",
                                "Incorrect date format")))));
    }

    @Test
    public void getReportBetweenDates_shouldReturnStatus400_ifCurrencyCodeIsNull() throws Exception {
        reportInfo.setCurrencyCode(null);
        mvc.perform(MockMvcRequestBuilders.post(GET_REPORT_BETWEEN_DATES_URI)
                .content(objectMapper.writeValueAsString(reportInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError("currencyCode",
                                "Enter currency code")))));
    }


    @Test
    public void getReportBetweenDates_shouldReturnStatus400_ifCurrencyCodeIsIncorrect() throws Exception {
        reportInfo.setCurrencyCode("USA");
        mvc.perform(MockMvcRequestBuilders.post(GET_REPORT_BETWEEN_DATES_URI)
                .content(objectMapper.writeValueAsString(reportInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString
                        (Arrays.asList(new FieldValidationError("currencyCode",
                                "Incorrect currency code")))));
    }

    @Test
    public void getReportBetweenDates_shouldReturnStatus200AndCorrectBody_ifCorrectRequestForReport() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(GET_REPORT_BETWEEN_DATES_URI)
                .content(objectMapper.writeValueAsString(reportInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andReturn();

        String jsonResponse = mvcResult.getResponse().getContentAsString();
        ReportForPeriod reportForPeriod = objectMapper.readValue(jsonResponse, ReportForPeriod.class);

        int actualStatus = mvcResult.getResponse().getStatus();
        String actualCurrencyCode = reportForPeriod.getCurrencyCode();
        BigDecimal actualSoldTotal = reportForPeriod.getSoldTotal();
        BigDecimal actualBoughtTotal = reportForPeriod.getBoughtTotal();
        int actualTotalTransactions = reportForPeriod.getTotalTransactions();
        List<DayReport> dayReports = reportForPeriod.getDayReports();

        Assertions.assertEquals(200, actualStatus);
        Assertions.assertEquals("USD", actualCurrencyCode);
        Assertions.assertEquals(new BigDecimal(600).setScale(2), actualSoldTotal);
        Assertions.assertEquals(new BigDecimal(0), actualBoughtTotal);
        Assertions.assertEquals(6, actualTotalTransactions);
        Assertions.assertEquals(3, dayReports.size());
    }

    @Test
    public void getReportBetweenDates_shouldReturnStatus403_ifWithoutJwt() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post(GET_REPORT_BETWEEN_DATES_URI)
                .content(objectMapper.writeValueAsString(reportInfo))
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getReportBetweenDates_shouldReturnStatus403_ifIncorrectJwt() throws Exception {
        String jwtKey = jwtUtil.getKey();
        jwtUtil.setKey("someKey");
        String wrongToken = "Bearer " + jwtUtil.generateAccessToken("user");
        jwtUtil.setKey(jwtKey);

        mvc.perform(MockMvcRequestBuilders.post(GET_REPORT_BETWEEN_DATES_URI)
                .content(objectMapper.writeValueAsString(reportInfo))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, wrongToken)
        ).andExpect(MockMvcResultMatchers.status().isForbidden());
    }

}