package com.example.controller;

import com.example.pojo.FieldValidationError;
import com.example.pojo.Message;
import com.example.pojo.User;
import com.example.security.JwtUtil;
import com.example.service.persistance.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:test.properties")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AuthControllerTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private JwtUtil jwtUtil;
    private static final String LOGIN_URI = "/auth/login";
    private static final String REGISTER_URI = "/auth/register";

    @BeforeAll
    public void addUsers() {
        User admin = new User("admin", "root");
        User user = new User("user", "root");
        userService.save(admin);
        userService.save(user);
    }

    @AfterAll
    public void deleteUsers() {
        userService.deleteByLogin("admin");
        userService.deleteByLogin("user");
    }

    @Test
    public void login_shouldReturnStatus200_ifCorrectCredentials() throws Exception {
        User user = new User("user", "root");
        mvc.perform(MockMvcRequestBuilders.post(LOGIN_URI)
                .content(objectMapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void login_shouldReturnStatus400_ifLoginIsNull() throws Exception {
        User user = new User(null, "root");
        mvc.perform(MockMvcRequestBuilders.post(LOGIN_URI)
                .content(objectMapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void login_shouldReturnStatus400_ifLoginIsEmpty() throws Exception {
        User user = new User("", "root");
        mvc.perform(MockMvcRequestBuilders.post(LOGIN_URI)
                .content(objectMapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void login_shouldReturnStatus400_ifPasswordIsNull() throws Exception {
        User user = new User("user", null);
        mvc.perform(MockMvcRequestBuilders.post(LOGIN_URI)
                .content(objectMapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void login_shouldReturnStatus400_ifPasswordIsEmpty() throws Exception {
        User user = new User("user", "");
        mvc.perform(MockMvcRequestBuilders.post(LOGIN_URI)
                .content(objectMapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void login_shouldReturnStatus400_ifLoginIsLongerThan50Characters() throws Exception {
        User user = new User();
        user.setLogin("123456789 123456789 123456789 123456789 123456789 x");
        user.setPassword("root");
        mvc.perform(MockMvcRequestBuilders.post(LOGIN_URI)
                .content(objectMapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void login_shouldReturnBody_ifLoginIsEmpty() throws Exception {
        User user = new User("", "root");
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(LOGIN_URI)
                .content(objectMapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String jsonResponse = mvcResult.getResponse().getContentAsString();
        FieldValidationError[] actualResponse = objectMapper.readValue(jsonResponse, FieldValidationError[].class);
        String expectedField = "login";
        String expectedMessage = "Enter login";

        Assertions.assertTrue(actualResponse[0].getField().equals(expectedField)
                && actualResponse[0].getMessage().equals(expectedMessage));
    }

    @Test
    public void login_shouldReturnBody_ifLoginLongerThan50Characters() throws Exception {
        User user = new User("123456789 123456789 123456789 123456789 123456789 x",
                "root");
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(LOGIN_URI)
                .content(objectMapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String jsonResponse = mvcResult.getResponse().getContentAsString();
        FieldValidationError[] actualResponse = objectMapper.readValue(jsonResponse, FieldValidationError[].class);
        String expectedField = "login";
        String expectedMessage = "Login mustn't be longer than 50 characters";

        Assertions.assertTrue(actualResponse[0].getField().equals(expectedField)
                && actualResponse[0].getMessage().equals(expectedMessage));
    }

    @Test
    public void login_shouldReturnBody_ifPasswordIsEmpty() throws Exception {
        User user = new User("user", "");
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(LOGIN_URI)
                .content(objectMapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String jsonResponse = mvcResult.getResponse().getContentAsString();
        FieldValidationError[] actualResponse = objectMapper.readValue(jsonResponse, FieldValidationError[].class);
        String expectedField = "password";
        String expectedMessage = "Enter password";

        Assertions.assertTrue(actualResponse[0].getField().equals(expectedField)
                && actualResponse[0].getMessage().equals(expectedMessage));
    }

    @Test
    public void login_shouldReturnStatus401_ifLoginNotExist() throws Exception {
        User user = new User("incorrectUser", "root");
        mvc.perform(MockMvcRequestBuilders.post(LOGIN_URI)
                .content(objectMapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void login_shouldReturnStatus401_ifIncorrectPassword() throws Exception {
        User user = new User("user", "****");
        mvc.perform(MockMvcRequestBuilders.post(LOGIN_URI)
                .content(objectMapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void login_shouldReturnToken_ifCorrectCredentials() throws Exception {
        User user = new User("user", "root");
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(LOGIN_URI)
                .content(objectMapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        String token = mvcResult.getResponse().getHeader(HttpHeaders.AUTHORIZATION);
        if (token != null && token.startsWith("Bearer ")) {
            token = token.substring(7);
        }
        Assertions.assertTrue(jwtUtil.validate(token));
    }

    @Test
    public void register_shouldReturnStatus400_ifUserExists() throws Exception {
        User user = new User("user", "root");
        String adminToken = "Bearer " + jwtUtil.generateAccessToken("admin");
        mvc.perform(MockMvcRequestBuilders.post(REGISTER_URI)
                .content(objectMapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, adminToken)
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content()
                        .json(objectMapper.writeValueAsString(new Message("User already exists"))));
    }

    @Test
    public void register_shouldReturnStatus403_ifUserIsNotAdmin() throws Exception {
        User user = new User("newUser", "root");
        String userToken = "Bearer " + jwtUtil.generateAccessToken("user");
        mvc.perform(MockMvcRequestBuilders.post(REGISTER_URI)
                .content(objectMapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, userToken)
        ).andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void register_shouldReturnStatus200_ifNewUserWasSaved() throws Exception {
        User user = new User("newUser", "root");
        String adminToken = "Bearer " + jwtUtil.generateAccessToken("admin");
        mvc.perform(MockMvcRequestBuilders.post(REGISTER_URI)
                .content(objectMapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, adminToken)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .json(objectMapper.writeValueAsString(new Message("User has been saved"))));
    }

}